package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

class Bank implements BankOperation {

    private static int accountNumber = 0;
    private static List<Account> accountList = new ArrayList<>();

    public void sampleList(List<Account> accounts) {
        accountList.addAll(accounts);
        accountNumber = accounts.size();

    }

    public int createAccount() {
        accountList.add(new Account(++accountNumber,0));
        return accountNumber;
    }

    public int deleteAccount(final int accountNumber) {
        int balance;
        for(Account account: accountList) {
            if(accountNumber==account.getAccountNumber()){
                balance = account.getAmount();
                accountList.removeIf(accNumber -> accNumber.getAccountNumber() == accountNumber);
                return balance;
            }else{
                return Bank.ACCOUNT_NOT_EXISTS;
            }
        }
        return 0;
    }

    public boolean deposit(int accountNumber, int amount) {
        if(amount<=0){
            return false;
        }
        for(Account account: accountList) {
            if(accountNumber==account.getAccountNumber()){
                account.setAmount(account.getAmount()+amount);
                return true;
            }
        }
        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        if(amount<=0){
            return false;
        }
        for(Account account: accountList) {
            if(accountNumber == account.getAccountNumber() && amount <= account.getAmount()){
                account.setAmount(account.getAmount()-amount);
                return true;
            }
        }
        return false;
    }

    public boolean transfer(int fromAccount, int toAccount, int amount) {
        if(amount<=0 || fromAccount == toAccount){
            return false;
        }
        Account accountFrom = null;
        Account accountTo = null;
        for(Account account: accountList){
            if(account.getAccountNumber() == fromAccount){
                accountFrom = account;
            }else if(account.getAccountNumber() == toAccount){
                accountTo = account;
            }
        }
        if(accountFrom==null || accountTo==null){
            return false;
        }else{
            if(accountFrom.getAmount()>=amount){
                accountFrom.setAmount(accountFrom.getAmount()-amount);
                accountTo.setAmount(accountTo.getAmount()+amount);
                return true;
            }else{
                return false;
            }
        }
    }

    public int accountBalance(int accountNumber) {
        for(Account account: accountList) {
            if(accountNumber==account.getAccountNumber()){
                return account.getAmount();
            }
        }
        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        int sum = 0;
        for(Account account: accountList) {
            sum += account.getAmount();
        }
        return sum;
    }
}
