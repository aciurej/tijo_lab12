package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class AccountList {
    public static List<Account> getAccounts(){
        List<Account> accounts = new ArrayList<Account>();
        accounts.add(new Account(1,100));
        accounts.add(new Account(2,300));
        accounts.add(new Account(3,90));
        accounts.add(new Account(4,780));
        return accounts;
    }
}
