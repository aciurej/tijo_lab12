package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DepositSpec extends Specification {

    @Unroll
    def "should deposit to #accountNumber"() {

        given: "initial data"
            def bank = new Bank()
            def accounts = AccountList.getAccounts()
            bank.sampleList(accounts)
        when: "deposit amount to accountNumber"
            def deposit = bank.deposit(accountNumber,amount)
        then: "check amount"
            deposit

        where:
        accountNumber | amount
        1             | 100
        2             | 300
        3             | 90
        4             | 780
    }
}
