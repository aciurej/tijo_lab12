package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class TransferSpec extends Specification {

    @Unroll
    def "should transfer #amount from #fromAccount to #toAccount"() {

        given: "initial data"
            def bank = new Bank()
            def accounts = AccountList.getAccounts()
            bank.sampleList(accounts)
        when: "transfer amount from fromAccount to toAccount"
            def transfer = bank.transfer(fromAccount,toAccount,amount)
        then: "check if can transfer"
            transfer

        where:
        fromAccount | toAccount | amount
        1           | 2         | 30
        2           | 3         | 90
        3           | 4         | 50
        4           | 1         | 30
    }
}
