package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountSpec extends Specification {

    @Unroll
    def "should sum balance"() {

        given: "initial data"
            def bank = new Bank()
            def accounts = AccountList.getAccounts()
            bank.sampleList(accounts)
        when: "summed balance"
            def sum = bank.sumAccountsBalance()
        then: "check if sum is valid"
            sum == amount

        where:
        accountNumber | amount
        1             | 1270
    }
}
