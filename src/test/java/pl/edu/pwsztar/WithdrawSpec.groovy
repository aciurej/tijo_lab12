package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class WithdrawSpec extends Specification {

    @Unroll
    def "should withdraw #amount from #accountNumber"() {

        given: "initial data"
            def bank = new Bank()
            def accounts = AccountList.getAccounts()
            bank.sampleList(accounts)
        when: "withdraw cash"
            def withdraw = bank.withdraw(accountNumber,amount)
        then: "check if can withdraw"
            withdraw

        where:
        accountNumber | amount
        1             | 80
        2             | 300
        3             | 120
        4             | 50
    }
}
