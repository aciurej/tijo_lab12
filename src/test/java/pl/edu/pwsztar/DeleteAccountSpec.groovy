package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    @Unroll
    def "should delete account number: #accountNumber amount: #amount"() {

        given: "initial data"
            def bank = new Bank()
            def accounts = AccountList.getAccounts()
            bank.sampleList(accounts)
        when: "account is deleted"
            def returnAmount = bank.deleteAccount(accountNumber);
        then: "check if return same amount"
            returnAmount == amount

        where:
        accountNumber | amount
        1             | 100
        2             | 300
        3             | 90
        4             | 780
        5             | -1
    }
}
