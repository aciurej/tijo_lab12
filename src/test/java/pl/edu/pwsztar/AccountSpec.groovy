package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class AccountSpec extends Specification {

    @Unroll
    def "should show #accountNumber amount: #amount"() {

        given: "initial data"
            def bank = new Bank()
            def accounts = AccountList.getAccounts()
            bank.sampleList(accounts)
        when: "show amount from accountNumber"
            def show = bank.accountBalance(accountNumber);
        then: "check if account amount is same"
            show == amount

        where:
        accountNumber | amount
        1             | 100
        2             | 300
        3             | 90
        4             | 780
        5             | -1
    }
}
